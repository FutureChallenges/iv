<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{


    public function _initDatabase()
    {
        $config = new Zend_Config_Ini('../application/configs/application.ini', 'production');

        $db = Zend_Db::factory($config->resources->db->adapter, array(
            'host'             => $config->resources->db->params->host,
            'username'         => $config->resources->db->params->username,
            'password'         => $config->resources->db->params->password,
            'dbname'           => $config->resources->db->params->dbname,
        ));

        Zend_Registry::set( 'db', $db );
    }
    protected function _initConfig()
    {
        $config = new Zend_Config($this->getOptions());
        Zend_Registry::set('config', $config);
        return $config;
    }

    public function _initUser() {

        $this->bootstrap ( 'layout' );
        $layout = $this->getResource ( 'layout' );
        $view = $layout->getView();

        $login = new Zend_Session_Namespace('login');
        if($login->user_id) {
            $db = Zend_Registry::get('db');
            $view->user_id = $login->user_id;
        }
    }

    public function _initViewHelpers()
    {
        $this->bootstrap('layout');
        $layout = $this->getResource('layout');
        $view = $layout->getView();
    }

    /**
     * Rewrite
     */
    public function _initRewrite() {
        $ctrl = Zend_Controller_Front::getInstance();
        $router = $ctrl->getRouter();
        $router->addRoute(
            'admin_uploadedfile_state',
            new Zend_Controller_Router_Route('admin/uploadedfiles/:uploadid/:state',
                array(
                    'controller' => 'admin',
                    'action' => 'changefilestate'
                )
            )
        );
        $router->addRoute(
            'admin_blog_edit',
            new Zend_Controller_Router_Route('admin/blogedit/:blogId',
                array(
                    'controller' => 'admin',
                    'action' => 'blogedit'
                )
            )
        );
        $router->addRoute(
            'admin_blog_del',
            new Zend_Controller_Router_Route('admin/blogdelete/:blogId',
                array(
                    'controller' => 'admin',
                    'action' => 'blogdelete'
                )
            )
        );
        $router->addRoute(
            'video_view',
            new Zend_Controller_Router_Route('videos/view/:uploadid',
                array(
                    'controller' => 'videos',
                    'action' => 'view'
                )
            )
        );
        $router->addRoute(
            'video_edit',
            new Zend_Controller_Router_Route('videos/edit/:uploadid',
                array(
                    'controller' => 'videos',
                    'action' => 'edit'
                )
            )
        );
        $router->addRoute(
            'video_verify',
            new Zend_Controller_Router_Route('videos/verify/:uploadid',
                array(
                    'controller' => 'videos',
                    'action' => 'verify'
                )
            )
        );
        $router->addRoute(
            'video_report',
            new Zend_Controller_Router_Route('videos/report/:uploadid',
                array(
                    'controller' => 'videos',
                    'action' => 'report'
                )
            )
        );
        $router->addRoute(
            'blog_view',
            new Zend_Controller_Router_Route('blog/view/:articleid',
                array(
                    'controller' => 'blog',
                    'action' => 'detail'
                )
            )
        );


    }
}

