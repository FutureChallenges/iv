<?php
/**
 * ganz wichtig für YT
 * extension=php_openssl.dll
 * allow_url_include = On
 */
class AdminController extends Zend_Controller_Action
{
    protected $_youtubeDataApiKey;  // API key for YouTube Data API v3
    protected $_youtubeUsername; // YT Username
    protected $_youtubePassword; // YT Password

    public function init()
    {
        $this->_youtubeDataApiKey = 'AIzaSyA-qtKNBpU-ESnQ6cauZGbETF1VG0ZkRic';
        $this->_youtubeUsername = 'blogger@collaboratory.de';
        $this->_youtubePassword = 'blogger2012';
        /* Initialize action controller here */

        $adminUserIds = array('1', '12','13');
        $login = new Zend_Session_Namespace('login');
        if(in_array($login->user_id, $adminUserIds) === false) {
            $this->_helper->flashMessenger->addMessage(array('error'=>'No Access.'));
            $this->_helper->redirector('index', 'index');
        }
    }

    public function indexAction()
    {


        // action body
    }

    /**
     * Blog Administration
     */
    public function blogAction()
    {

        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('blog' => 'blog_articles'));
        $select->join(array('user' => 'users'), 'blog.article_author = user.user_id');
        $select->order('blog.article_id DESC');
        $this->view->blogitems = $db->fetchAll($select);
    }
    /**
     * Blog Edit Administration
     */
    public function blogeditAction()
    {
        $blogId = $this->getRequest()->getParam('blogId');

        $db = Zend_Registry::get('db');

        if($this->getRequest()->getMethod() == 'POST') {
            $params = $this->getRequest()->getParams();

            $login = new Zend_Session_Namespace('login');
            if($blogId == "0") {
                $uploadPath = APPLICATION_PATH.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'blog'.DIRECTORY_SEPARATOR;
                echo $uploadPath;

                $primaryImage = $_FILES['image'];
                $secondaryImage = $_FILES['secimage'];

                $insertArr = array(
                    'article_title' => $params['title'],
                    'article_content' => $params['content'],
                    'article_author' => $login->user_id,
                    'article_date' => new Zend_Db_Expr('NOW()')
                );

                if(isset($primaryImage['tmp_name'])) {
                    $filename = uniqid().'-'.$primaryImage['name'];
                    move_uploaded_file($primaryImage['tmp_name'],  $uploadPath.$filename);
                    $insertArr['article_image'] = $filename;
                } else {
                    $insertArr['article_image'] = null;
                }
                if(isset($secondaryImage['tmp_name'])) {
                    $filename = uniqid().'-'.$secondaryImage['name'];
                    move_uploaded_file($secondaryImage['tmp_name'],  $uploadPath.$filename);
                    $insertArr['article_secimage'] = $filename;
                } else {
                    $insertArr['article_secimage'] = null;
                }

                // new

                $db->insert('blog_articles', $insertArr);
            } else {
                // update
                $updateArr = array(
                    'article_title' => $params['title'],
                    'article_content' => $params['content']
                );
                $db->update('blog_articles', $updateArr, 'article_id = '.$blogId);
            }

            // redirect if success
            $this->_helper->redirector('blog', 'admin');
        }

        $select = $db->select();
        $select->from(array('blog' => 'blog_articles'));
        $select->join(array('user' => 'users'), 'blog.article_author = user.user_id');
        $select->where('blog.article_id = ?', $blogId);
        $this->view->blogdata = $db->fetchRow($select);
        $this->view->blogid  = $blogId;
    }

    /**
     * Blogentry delete
     */
    public function blogdeleteAction()
    {
        $db = Zend_Registry::get('db');
        $blogId = $this->getRequest()->getParam('blogId');
        // db: delete
        $db->delete('blog_articles', array('article_id = ' . $blogId));

        // redirect if success
        $this->_helper->flashMessenger->addMessage(array('success'=>'Entry deleted.'));
        $this->_helper->redirector('blog', 'admin');
    }


    /**
     * list uploaded files
     */
    public function uploadedfilesAction()
    {
        $this->view->files = $this->getUploadedFiles();
    }

    /*
     * get uploaded files
     * @return mixed
     */
    public function getUploadedFiles()
    {
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('upload' => 'uploads'));
        $select->join(array('user' => 'users'), 'upload.upload_userid = user.user_id');
        $select->where('upload_published IS NULL');
        $select->order('upload.upload_id DESC');

        return $db->fetchAll($select);
    }

    /**
     * change file state for uploaded files
     *
     */
    public function changefilestateAction() {
        // get params
        $uploadId = $this->getRequest()->getParam('uploadid');
        $state = $this->getRequest()->getParam('state');

        // kill hack0rs
        if(empty($uploadId) || empty($state)) {
            $this->_helper->flashMessenger->addMessage(array('error'=>'Fehler, UploadID oder State fehlt'));
            $this->_helper->redirector('admin', 'uploadedfiles');
        }

        // init db
        $db = Zend_Registry::get('db');

        // get video detail
        $select = $db->select();
        $select->from('uploads');
        $select->where('upload_id = ?', $uploadId);
        $videoData = $db->fetchRow($select);

        // state only delete
        if($state == "delete") {

            $url = $videoData['upload_filepicker_url'].'?key=AAALbRX6wQOenTQDNy9Mjz';
            $cmd = "curl -X DELETE '".$url."'";
            exec($cmd);

            /*$ch = curl_init($url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            $response = curl_exec($ch);
            var_dump($url, $response, $ch);
            die();
            */

            // db: delete
            $db->delete('uploads', array('upload_id = ' . $uploadId));

            // redirect if success
            $this->_helper->flashMessenger->addMessage(array('success'=>'Video gelöscht.'));
            $this->_helper->redirector('uploadedfiles', 'admin');
        }

        if($state == "uploadyt") {

            // download file OVER file_get_contents
            // TODO: change method file_get_contents to CURL?
            $downloadUrl  = $videoData['upload_filepicker_url'];

            $downloadPath = APPLICATION_PATH.DIRECTORY_SEPARATOR.'..'.
                DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.uniqid().'-'.$videoData['upload_filepicker_filename'];

            // download from filepicker
            $data = file_get_contents($downloadUrl);
            file_put_contents($downloadPath, $data);


            // upload YT
            //$this->uploadYoutube($downloadPath)
            // TODO: refactor to own function

            $authenticationURL= 'https://www.google.com/accounts/ClientLogin';
            $httpClient =
                Zend_Gdata_ClientLogin::getHttpClient(
                    $username = $this->_youtubeUsername,
                    $password = $this->_youtubePassword,
                    $service = 'youtube',
                    $client = null,
                    $source = 'ivoices', // a short string identifying your application
                    $loginToken = null,
                    $loginCaptcha = null,
                    $authenticationURL);
            try {
                $yt = new Zend_Gdata_YouTube($httpClient, 'IVOICES UPLOAD', 'IVOICES', $this->_youtubeDataApiKey);
            } catch(Exception $e) {
                die($e->getMessage());
            }

            // create a new VideoEntry object
            $myVideoEntry = new Zend_Gdata_YouTube_VideoEntry();
            $file = realpath($downloadPath);


            $filesource = $yt->newMediaFileSource($file);
            $filesource->setContentType($videoData['upload_filepicker_mimetype']);
//            $filesource->setContentType('video/mpg');
            $filesource->setSlug($file);

            // add the filesource to the video entry
            $myVideoEntry->setMediaSource($filesource);
            $myVideoEntry->setVideoPrivate();
            $myVideoEntry->setVideoTitle($videoData['upload_title']);
            $myVideoEntry->setVideoDescription($videoData['upload_title']);
            // The category must be a valid YouTube category!
            $myVideoEntry->setVideoCategory('Nonprofit');

            // Set keywords. Please note that this must be a comma-separated string
            // and that individual keywords cannot contain whitespace
            if($videoData['upload_tags']) {
                $myVideoEntry->SetVideoTags($videoData['upload_tags']);
            }

            // TODO: GEO TAG?
            // set the video's location -- this is also optional
            //$yt->registerPackage('Zend_Gdata_Geo');
            //$yt->registerPackage('Zend_Gdata_Geo_Extension');
            //$where = $yt->newGeoRssWhere();
            //$position = $yt->newGmlPos('37.0 -122.0');
            //$where->point = $yt->newGmlPoint($position);
            //$myVideoEntry->setWhere($where);

            // upload URI for the currently authenticated user
            $uploadUrl = 'http://uploads.gdata.youtube.com/feeds/api/users/default/uploads';

            // try to upload the video, catching a Zend_Gdata_App_HttpException,
            // if available, or just a regular Zend_Gdata_App_Exception otherwise
            try {
                $newEntry = $yt->insertEntry($myVideoEntry, $uploadUrl, 'Zend_Gdata_YouTube_VideoEntry');
            } catch (Zend_Gdata_App_HttpException $httpException) {
                echo "A" . $httpException->getRawResponseBody();
            } catch (Zend_Gdata_App_Exception $e) {
                echo "B" . $e->getMessage();
            }

            // remove tmp file
            unlink($downloadPath);

            if(isset($newEntry)) {
                // update entry into the db
                $data = array(
                    'upload_active' => '1',
                    'upload_published' => $newEntry->getVideoId(),
                    'upload_published_date' => new Zend_Db_Expr('NOW()')
                );
                $db->update('uploads', $data, 'upload_id = '.$uploadId);

                // delete from filepicker
                $url = $videoData['upload_filepicker_url'].'?key=AAALbRX6wQOenTQDNy9Mjz';
                $cmd = "curl -X DELETE '".$url."'";
                exec($cmd);

                // redirect if success
                $this->_helper->flashMessenger->addMessage(array('success'=>'Video wurde auf Youtube hochgeladen, bitte entnehmen Sie weiteres auf YT.'));
                $this->_helper->redirector('uploadedfiles', 'admin');
            } else {
                $this->_helper->flashMessenger->addMessage(array('error'=>'Es gibt ein Problem mit dem Upload.'));
                $this->_helper->redirector('uploadedfiles', 'admin');
            }
        }
    }

    private function get_mime($file) {
        if (function_exists("finfo_file")) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
            $mime = finfo_file($finfo, $file);
            finfo_close($finfo);
            return $mime;
        } else if (function_exists("mime_content_type")) {
            return mime_content_type($file);
        } else if (!stristr(ini_get("disable_functions"), "shell_exec")) {
            // http://stackoverflow.com/a/134930/1593459
            $file = escapeshellarg($file);
            $mime = shell_exec("file -bi " . $file);
            return $mime;
        } else {
            return false;
        }
    }
}