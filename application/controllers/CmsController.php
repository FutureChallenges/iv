<?php

class CmsController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }

    public function contactAction()
    {
        $this->view->headTitle('Contact Us');

        $params = $this->getRequest()->getParams('contact');
        if(isset($params['contact'])) {
            $params = $params['contact'];
        }

        if($this->getRequest()->getMethod() == 'POST') {
            if(empty($params['message']) || empty($params['email']) || empty($params['name'])) {
                $this->_helper->flashMessenger->addMessage(array('error'=> 'Please fill all fields.' ));
                $this->_helper->redirector('contact', 'cms');
            }

            $mail = new Zend_Mail();
            $mail->setBodyText($params['message']);
            $mail->setFrom($params['email'], $params['name']);
            $mail->addTo('info@irrepressiblevoices.org');
            $mail->setSubject('Contactform from '.$params['name'].' - '.date('d.m.Y - H:i:s'));
            $mail->send();

            $this->view->submitted = true;
        }
    }

    public function aboutAction()
    {
        $this->view->headTitle('About us');
    }

    public function termsAction()
    {
        $this->view->headTitle('Terms and conditions');
    }

    public function visionAction()
    {
        $this->view->headTitle('Our Vision');
    }

    public function historyAction()
    {
        $this->view->headTitle('History');
    }

    public function involvedAction()
    {
        $this->view->headTitle('Get Involved!');
    }

    public function legalAction()
    {
        $this->view->headTitle('Legal');
    }

}
