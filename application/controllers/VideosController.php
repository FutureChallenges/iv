<?php

class VideosController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_categories = array(
            'Death penalty',
            'Armed Conflict',
            'Arms control',
            'Childrens Rights',
            'Detention and Imprisonment',
            'Discrimination',
            'Economic, Social and Cultural Rights',
            'Enforced Disappearances',
            'Freedom of Expression',
            'Human Rights Defenders',
            'Human Rights Education',
            'Impunity',
            'Indigenous Peoples',
            'Poverty',
            'Refugees, Migrants and Internally Displaced Persons',
            'Religion',
            'Torture'
        );
    }

    public function indexAction()
    {
        $yt = new Zend_Gdata_YouTube();
        $this->view->videofeed = $yt->getUserUploads('irrepressiblevoices');
    }

    public function viewAction()
    {
        $uploadId = $this->getRequest()->getParam('uploadid');

        $db = Zend_Registry::get('db');

        $login = new Zend_Session_Namespace('login');
        if($this->getRequest()->getMethod() == 'POST') {
            $params = $this->getRequest()->getParams();

            if(empty($params['message'])) {
                $this->_helper->flashMessenger->addMessage(array('error'=> 'Please add a comment.' ));
                $this->_helper->redirector('view', 'videos', NULL, array('uploadid' => $uploadId));
            }

            $insertData = array(
                'comment_uploadid' => $uploadId,
                'comment_userid' => $login->user_id,
                'comment_date' => new Zend_Db_Expr('NOW()'),
                'comment_message' => $params['message']
            );
            $db->insert('upload_comments', $insertData);

            //TODO
            //$this->_helper->flashMessenger->addMessage(array('success'=> 'Please add a comment.' ));
            $this->_helper->redirector('view', 'videos', NULL, array('uploadid' => $uploadId));
        }

        $select = $db->select();
        $select->from(array('upl' => 'uploads'));
        $select->where('upload_published = ?', $uploadId);
        $results = $db->fetchRow($select);

        $defaultVideos = array(
            'UBOJljxaB1U', 'dYfuO5ndKRM', 'MdyW-4uLPYA', 'N6AlYpGn-us', 'PuR1potEvGQ',
            'J1HjujCsmek', 'txIDa6qD9ls', 'Gx-R2lfg9UI'
        );

        if((in_array($uploadId, $defaultVideos) === false) && ($results === false)) {
            $this->_helper->flashMessenger->addMessage(array('error'=> 'Video not found.' ));
            $this->_helper->redirector('index', 'videos');
        } else {
            $yt = new Zend_Gdata_YouTube();
            $this->view->video = $videoEntry = $yt->getVideoEntry($uploadId);
            $this->view->videodata = $results;

            // comments
            $select = $db->select();
            $select->from(array('upload' => 'upload_comments'));
            $select->where('upload.comment_uploadid = ?', $uploadId);
            $select->join(array('user' => 'users'), 'upload.comment_userid = user.user_id');
            $select->order('upload.comment_date DESC');
            $this->view->comments = $db->fetchAll($select);


            // verified
            $select = $db->select();
            $select->from(array('uplv' => 'upload_verifying'));
            $select->where('verify_uploadid = ?', $uploadId);
            $this->view->verfied = $db->fetchAll($select);

            if( $login->user_id ) {
                $select = $db->select();

                $select->from(array('uplv' => 'upload_verifying'));
                $select->where('uplv.verify_uploadid = ?', $uploadId);
                $select->where('uplv.verify_userid = ?', $login->user_id);
                $this->view->verfied_self = $db->fetchRow($select);
            }
        }
    }

    /**
     * edit
     */
    public function editAction()
    {
        $uploadId = $this->getRequest()->getParam('uploadid');

        $db = Zend_Registry::get('db');
        $this->view->categories = $this->_categories;
        $login = new Zend_Session_Namespace('login');
        if(!isset($login->user_id)) {
            $this->_helper->redirector('view', 'videos', NULL, array('uploadid' => $uploadId));
        } else {
            $select = $db->select();
            $select->from(array('upl' => 'uploads'));
            $select->where('upload_published = ?', $uploadId);
            $results = $db->fetchRow($select);
            $this->view->videodata = $results;

            if($this->getRequest()->getMethod() == 'POST') {
                $params = $this->getRequest()->getParams();

                // data
                $updateArr = array(
                    'upload_place' => $params['place'],
                    'upload_recorded' => $params['recorded'],
                    'upload_tags' => $params['tags'],
                    'upload_desc' => $params['desc'],
                    'upload_issue' => $params['issue'],
                    'upload_statement' => $params['statement']
                );
                $db->update('uploads', $updateArr, 'upload_id = '.$params['upload_id']);

                // redirect if success
                $this->_helper->flashMessenger->addMessage(array('success'=> 'Thank you for editing.' ));
                $this->_helper->redirector('view', 'videos', NULL, array('uploadid' => $uploadId));
            }

            //die();
        }
    }

    /**
     * verify
     */
    public function verifyAction()
    {
        $uploadId = $this->getRequest()->getParam('uploadid');
        $db = Zend_Registry::get('db');

        $login = new Zend_Session_Namespace('login');
        if($login->user_id && isset($uploadId)) {
            // insert
            $insertData = array(
                'verify_uploadid' => $uploadId,
                'verify_date' => new Zend_Db_Expr('NOW()'),
                'verify_userid' => $login->user_id
            );
            $db->insert('upload_verifying', $insertData);

            // redirect
            $this->_helper->flashMessenger->addMessage(array('success'=> 'Thank you for verifying.' ));
            $this->_helper->redirector('view', 'videos', NULL, array('uploadid' => $uploadId));
        }

    }

    /**
     * report
     */
    public function reportAction()
    {
        $uploadId = $this->getRequest()->getParam('uploadid');
        //$db = Zend_Registry::get('db');

        $login = new Zend_Session_Namespace('login');
        if($login->user_id && isset($uploadId)) {
            // mail
            $mail = new Zend_Mail();
            $mail->setBodyText('REPORTING');
            $mail->setFrom('nobody@irrepressiblevoices.org');
            $mail->addTo('info@irrepressiblevoices.org');
            $mail->setSubject('REPORT '.$uploadId.' - '.date('d.m.Y - H:i:s'));
            $mail->send();

            // redirect
            $this->_helper->flashMessenger->addMessage(array('success'=> 'Thank you for your report.' ));
            $this->_helper->redirector('view', 'videos', NULL, array('uploadid' => $uploadId));
        }

    }
}
