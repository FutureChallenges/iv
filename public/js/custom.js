
$(document).ready(function(){

    $('.carousel').carousel();

    if($('#blogedit_textarea').length > 0){
        $('#blogedit_textarea').wysihtml5();
    }


    if($('#socialshareprivacy').length > 0){
        $('#socialshareprivacy').socialSharePrivacy({
            services : {
                facebook : {
                    'language': 'en_US'
                },
                twitter : {
                    'language' : 'en'
                },
                gplus : {
                    'language' : 'en'
                }
            },
            info_link: ''
        });
    }

    if($('fieldset a').length > 0){
       $('fieldset a').tooltip();
    }

    $("#partners").jCarouselLite({
        btnNext: ".next",
        btnPrev: ".prev",
        auto: 800,
        speed: 1000
    });


    if($('#filepick').length > 0){

        filepicker.setKey('AAALbRX6wQOenTQDNy9Mjz');

        $('#filepick').change(function(){
            var output = $("#ajax-upload-output");
            filepicker.store(this, {
                location: 'S3',
                container: 'modal',
                extensions: ['.mov', '.mpeg4', '.mpeg','avi','.wmv',',mpegps','.mpg','.flv','.3gp','.3gpp','.webm']



        }, function(FPFile){
                output.html('Uploaded: '+FPFile.filename+'');
                $('#filepicker_url').val(FPFile.url);
                $('#filepicker_filename').val(FPFile.filename);
                $('#filepicker_mimetype').val(FPFile.mimetype);
                $('#filepicker_size').val(FPFile.size);

                if(FPFile.url != "") {
                    $('#submitbtn').removeAttr('disabled');
                }
                $('#filepick').attr('disabled', 'disabled');

            }, function(fperror){
                $('#filepick').attr('disabled', 'disabled');
                output.text(fperror.toString());
            }, function(progress){
                output.text("Uploading... ("+progress+"%)");
            });
        });
    }
});

// init
function initialize() {
    var mapOptions = {
        center: new google.maps.LatLng(-33.8688, 151.2195),
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById('map_canvas'),
        mapOptions);

    var input = document.getElementById('searchTextField');
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);
    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
        map: map
    });
}
if (typeof(google) != "undefined") {
    google.maps.event.addDomListener(window, 'load', initialize);
}

//Bind this keypress function to all of the input tags
jQuery("input").keypress(function (evt) {
//Deterime where our character code is coming from within the event
    var charCode = evt.charCode || evt.keyCode;
    if (charCode  == 13) { //Enter key's keycode
        return false;
    }
});
$('#info_checkbox').modal({
    keyboard: false,
    show: false
});
$('#info_report').modal({
    keyboard: false,
    show: false
})

jQuery("form[name=submit]").bind('submit',function(){

    var checkbox = jQuery('#in').attr('checked');
    var place = jQuery('#searchTextField').val();
    var title = jQuery('#title').val();

    if(title == "") {
        alert('In order to submit your video, you must complete "title".');
        return false;
    }
    if(place == "") {
        alert('In order to submit your video, you must complete "place".');
        return false;
    }

    if(checkbox == "checked") {
        return true;
    } else {
        $('#info_checkbox').modal('show');
        return false;
    }
    return false;
});
